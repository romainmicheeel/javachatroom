# JavaChatRoom

application de chat utilisant les Socket TCP.

## Concept:  

- Un client et un serveur communiquent grâce aux socket tcp
- les deux peuvent etre lancés dans la même machine ou sur deux machines qui peuvent communiquer en réseau

## Fonctionnement

```
java ChatServer <port>
java ChatClient <IP Adress> <port>
```

- Si les deux sont lancés sur la même machine, du coté client l'adresse sera la local host adress donc par exemple: 

`java ChatServer 2002`<br>
`java ChatClient 127.0.0.1 2002`

- si les les deux fonctionnent sur des machines différentes, on spécifie coté client l'adresse ip sur laquelle le serveur tourne

## Auteurs :

- Romain Michel
- Valentin Diologient
B2 Informatique