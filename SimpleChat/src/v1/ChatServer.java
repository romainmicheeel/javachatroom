package v1;

import java.net.*; 
import java.io.*; 
  
public class  ChatServer{ 
  public static void main(String args[]) throws Exception {
	  if(args.length != 1) {
			System.out.println("You need to specify a port number\nUSAGE: java ChatServer <port>");
		}else {
		    ServerSocket ss=new ServerSocket(Integer.parseInt(args[0])); 
		    Socket sk=ss.accept(); 
		    BufferedReader cin=new BufferedReader(new 
		InputStreamReader(sk.getInputStream())); 
		    PrintStream cout=new PrintStream(sk.getOutputStream()); 
		    BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in)); 
		    String s; 
		    while (  true ) 
		    { 
		      s=cin.readLine(); 
		        if (s.equalsIgnoreCase("END")) 
		        { 
		        cout.println("BYE"); 
		            break; 
		          } 
		      System. out.print("Client : "+s+"\n"); 
		      System.out.print("Server : "); 
		      s=stdin.readLine(); 
		      cout.println(s); 
		    } 
		    ss.close(); 
		     sk.close(); 
		     cin.close(); 
		    cout.close(); 
		     stdin.close(); 	
		} 
  }
}