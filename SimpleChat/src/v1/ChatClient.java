package v1;

import java.net.*; 
import java.io.*; 
public class  ChatClient
{ 
  public static void main(String args[]) throws Exception {
	if(args.length != 2) {
		System.out.println("You need to specify an adress ip and a port number\nUSAGE: java ChatClient <IP Adress> <port>");
	}else {
		Socket sk=new Socket(args[0],Integer.parseInt(args[1])); 
	    BufferedReader sin=new BufferedReader(new InputStreamReader(sk.getInputStream())); 
	    PrintStream sout=new PrintStream(sk.getOutputStream()); 
	    BufferedReader stdin=new BufferedReader(new InputStreamReader(System.in)); 
	    String s; 
	    while (  true ) 
	    { 
	      System.out.print("Client : "); 
	      s=stdin.readLine(); 
	      sout.println(s); 
	      s=sin.readLine(); 
	      System.out.print("Server : "+s+"\n"); 
	        if ( s.equalsIgnoreCase("BYE") ) 
	          break; 
	    } 
	     sk.close(); 
	     sin.close(); 
	     sout.close(); 
	     stdin.close(); 
	}
  } 
}